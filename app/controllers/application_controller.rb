class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
    render html: "hoi, wereld!"
  end
  def goodbye
    render html: "vaarwel, wereld :)!"
  end
end
